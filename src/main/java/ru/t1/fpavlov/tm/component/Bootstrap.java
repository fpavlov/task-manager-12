package ru.t1.fpavlov.tm.component;

import ru.t1.fpavlov.tm.api.controller.ICommandController;
import ru.t1.fpavlov.tm.api.controller.IProjectController;
import ru.t1.fpavlov.tm.api.controller.ITaskController;
import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.controller.*;
import ru.t1.fpavlov.tm.repository.*;
import ru.t1.fpavlov.tm.service.*;
import ru.t1.fpavlov.tm.util.TerminalUtil;


import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_PROJECT_LIST:
                projectController.showAll();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_PROJECT_FIND_BY_ID:
                projectController.showById();
                break;
            case CMD_PROJECT_FIND_BY_INDEX:
                projectController.showByIndex();
                break;
            case CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case CMD_PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case CMD_PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case CMD_TASK_CREATE:
                taskController.create();
                break;
            case CMD_TASK_LIST:
                taskController.showAll();
                break;
            case CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case CMD_TASK_FIND_BY_ID:
                taskController.showById();
                break;
            case CMD_TASK_FIND_BY_INDEX:
                taskController.showByIndex();
                break;
            case CMD_TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case CMD_TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case CMD_TASK_START_BY_ID:
                taskController.startById();
                break;
            case CMD_TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case CMD_TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                commandController.incorrectCommand();
        }
    }

    private void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                commandController.displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_SHORT_VERSION:
                commandController.displayVersion();
                break;
            case CMD_SHORT_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_SHORT_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_SHORT_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_SHORT_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_SHORT_PROJECT_LIST:
                projectController.showAll();
                break;
            case CMD_SHORT_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_SHORT_TASK_CREATE:
                taskController.create();
                break;
            case CMD_SHORT_TASK_LIST:
                taskController.showAll();
                break;
            case CMD_SHORT_TASK_CLEAR:
                taskController.clear();
                break;
            default:
                commandController.incorrectArgument();
        }

        System.exit(0);
    }

    public void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        String param;

        commandController.displayWelcomeText();

        while (true) {
            System.out.println("-- Please enter a command --");
            param = TerminalUtil.nextLine();
            listenerCommand(param);
        }
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void quite() {
        System.exit(0);
    }

}
