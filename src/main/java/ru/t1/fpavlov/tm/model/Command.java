package ru.t1.fpavlov.tm.model;

/*
 * Created by fpavlov on 28.09.2021.
 */
public final class Command {

    private String name;

    private String argument;

    private String description;

    public Command() {
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public Command(final String name, final String description) {
        this.name = name;
        this.argument = null;
        this.description = description;
    }

    @Override
    public String toString() {
        String formatMessage = "";

        if (this.name != null && !this.name.isEmpty()) {
            formatMessage += this.name;
        }

        if (this.argument != null && !this.argument.isEmpty()) {
            formatMessage += "," + this.argument;
        }

        if (this.description != null && !this.description.isEmpty()) {
            formatMessage += " - " + this.description;
        }

        return formatMessage;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
