package ru.t1.fpavlov.tm.model;

import ru.t1.fpavlov.tm.enumerated.Status;

import java.util.UUID;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }

    @Override
    public String toString() {
        return "".format(" |%40s |%20s |%20s |%20s |",
                this.id,
                this.name,
                this.description,
                Status.toName(this.status));
    }

}
