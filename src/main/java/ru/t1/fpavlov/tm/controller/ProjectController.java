package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.IProjectController;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Project item = projectService.create(name, description);
        if (item == null) {
            System.out.println("\tError!");
        } else {
            System.out.println("\tOk");
        }
    }

    @Override
    public void clear() {
        System.out.println("Clear projects");
        projectService.clear();
        System.out.println("\tOk");
    }

    @Override
    public void showAll() {
        int index = 1;
        final List<Project> items = projectService.findAll();
        System.out.println("Projects list:");
        for (final Project item : items) {
            System.out.format("|%2d%s%n", index, item);
            index++;
        }
        System.out.println("\tOk");
    }

    @Override
    public void removeById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        final Project item = projectService.removeById(itemId);
        if (item == null) {
            System.out.format("Error! Project with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Project with id(%s). Was " +
                    "removed successfully%n", itemId);
        }
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Project item = projectService.removeByIndex(itemIndex - 1);
        if (item == null) {
            System.out.format("Error! Project with index(%d)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Project with index(%d). Was " +
                    "removed successfully%n", itemIndex);
        }
    }

    @Override
    public void showById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        final Project item = projectService.findById(itemId);
        if (item == null) {
            System.out.format("Error! Project with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Project with id(%s). Was " +
                    "found successfully%n%s%n", itemId, item);
        }
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Project item = projectService.findByIndex(itemIndex - 1);

        if (item == null) {
            System.out.format("Error! Project with index(%d)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Project with index(%d). Was " +
                    "found successfully%n%s%n", itemIndex, item);
        }
    }

    @Override
    public void updateById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        final Project item = projectService.updateById(itemId, name, description);
        if (item == null) {
            System.out.format("Error! Project with id(%s)." +
                    "Wasn't found%n", itemId);
        } else {
            System.out.format("Project with id(%s). Was " +
                    "updated successfully%n%s%n", itemId, item);
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        final Project item = projectService.updateByIndex(itemIndex - 1, name, description);
        if (item == null) {
            System.out.format("Error! Project with index(%s)." +
                    "Wasn't found%n", itemIndex);
        } else {
            System.out.format("Project with index(%s). Was " +
                    "updated successfully%n%s%n", itemIndex, item);
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project item = this.projectService.changeStatusById(itemId, status);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project item = this.projectService.changeStatusByIndex(itemIndex, status);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        final Project item = this.projectService.changeStatusById(itemId, Status.IN_PROGRESS);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        final Project item = this.projectService.changeStatusByIndex(itemIndex, Status.IN_PROGRESS);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void completeById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        final Project item = this.projectService.changeStatusById(itemId, Status.COMPLETED);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

    @Override
    public void completeByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger() - 1;
        final Project item = this.projectService.changeStatusByIndex(itemIndex, Status.COMPLETED);
        if (item == null) System.out.println("Error! Status wasn't changed");
        else System.out.println("Status was changed successfully");
    }

}
