package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectService implements IProjectService {
    
    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return this.projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return this.add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return this.add(new Project(name, description));
    }

    @Override
    public void clear() {
        this.projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return this.projectRepository.findAll();
    }

    @Override
    public void remove(final Project project) {
        this.projectRepository.remove(project);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return this.projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= this.projectRepository.getSize()) return null;
        return this.projectRepository.findByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project item = this.findById(id);
        if (item == null) return null;
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (index >= this.projectRepository.getSize()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project item = this.findByIndex(index);
        if (item == null) return null;
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return this.projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= this.projectRepository.getSize()) return null;
        return this.projectRepository.removeByIndex(index);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        final Project item = this.findById(id);
        if (item == null) return null;
        item.setStatus(status);
        return item;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index >= this.projectRepository.getSize()) return null;
        if (status == null) return null;
        final Project item = this.findByIndex(index);
        if (item == null) return null;
        item.setStatus(status);
        return item;
    }

}
