package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return this.taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return this.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return this.add(new Task(name, description));
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void remove(final Task task) {
        this.taskRepository.remove(task);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return this.taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= this.taskRepository.getSize()) return null;
        return this.taskRepository.findByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task item = this.findById(id);
        if (item == null) return null;
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (index >= this.taskRepository.getSize()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task item = this.findByIndex(index);
        if (item == null) return null;
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return this.taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        if (index >= this.taskRepository.getSize()) return null;
        return this.taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        final Task item = this.findById(id);
        if (item == null) return null;
        item.setStatus(status);
        return item;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (index >= this.taskRepository.getSize()) return null;
        if (status == null) return null;
        final Task item = this.findByIndex(index);
        if (item == null) return null;
        item.setStatus(status);
        return item;
    }

}
