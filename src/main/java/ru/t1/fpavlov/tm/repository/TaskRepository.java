package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task project) {
        this.tasks.add(project);
        return project;
    }

    @Override
    public void clear() {
        this.tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return this.tasks;
    }

    @Override
    public Task findById(final String id) {
        Task itemFound = null;
        for (Task item : this.tasks) {
            if (item.getId().equals(id)) {
                itemFound = item;
                break;
            }
        }
        return itemFound;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return this.tasks.get(index);
    }

    @Override
    public void remove(Task task) {
        this.tasks.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        final Task item = this.findById(id);
        if (item == null) return null;
        this.remove(item);
        return item;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task item = this.findByIndex(index);
        if (item == null) return null;
        this.remove(item);
        return item;
    }

    @Override
    public int getSize() {
        return this.tasks.size();
    }

}
