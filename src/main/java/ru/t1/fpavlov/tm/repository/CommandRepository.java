package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.model.Command;

import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;


/*
 * Created by fpavlov on 04.10.2021.
 */
public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            CMD_ABOUT,
            CMD_SHORT_ABOUT,
            "Display developer info"
    );

    private final static Command HELP = new Command(
            CMD_HELP,
            CMD_SHORT_HELP,
            "Display list of terminal command"
    );

    private final static Command VERSION = new Command(
            CMD_VERSION,
            CMD_SHORT_VERSION,
            "Display program version"
    );

    private final static Command EXIT = new Command(
            CMD_EXIT,
            "Quite"
    );

    private final static Command INFO = new Command(
            CMD_INFO,
            CMD_SHORT_INFO,
            "Display available resource of system"
    );

    private final static Command ARGUMENTS = new Command(
            CMD_ARGUMENTS,
            CMD_SHORT_ARGUMENTS,
            "Display arguments of running"
    );

    private final static Command COMMANDS = new Command(
            CMD_COMMANDS,
            CMD_SHORT_COMMANDS,
            "Display names of available commands"
    );

    private final static Command PROJECT_CREATE = new Command(
            CMD_PROJECT_CREATE,
            CMD_SHORT_PROJECT_CREATE,
            "Create new project"

    );

    private final static Command PROJECT_CLEAR = new Command(
            CMD_PROJECT_CLEAR,
            CMD_SHORT_PROJECT_CLEAR,
            "Clear project list"
    );

    private final static Command PROJECT_LIST = new Command(
            CMD_PROJECT_LIST,
            CMD_SHORT_PROJECT_LIST,
            "Display available projects"
    );

    private final static Command PROJECT_FIND_BY_ID = new Command(
            CMD_PROJECT_FIND_BY_ID,
            "Find project by id"
    );

    private final static Command PROJECT_FIND_BY_INDEX = new Command(
            CMD_PROJECT_FIND_BY_INDEX,
            "Find project by index"
    );

    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            CMD_PROJECT_UPDATE_BY_ID,
            "Update project by id"
    );

    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            CMD_PROJECT_UPDATE_BY_INDEX,
            "Update project by index"
    );

    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            CMD_PROJECT_REMOVE_BY_ID,
            "Remove project by id"
    );

    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            CMD_PROJECT_REMOVE_BY_INDEX,
            "Remove project by index"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            CMD_PROJECT_CHANGE_STATUS_BY_ID,
            "Change project status by id"
    );

    private final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            CMD_PROJECT_CHANGE_STATUS_BY_INDEX,
            "Change project status by index"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            CMD_PROJECT_START_BY_ID,
            "Start project by id"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            CMD_PROJECT_START_BY_INDEX,
            "Start project by index"
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            CMD_PROJECT_COMPLETE_BY_ID,
            "Complete project by id"
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            CMD_PROJECT_COMPLETE_BY_INDEX,
            "Complete project by index"
    );

    private final static Command TASK_CREATE = new Command(
            CMD_TASK_CREATE,
            CMD_SHORT_TASK_CREATE,
            "Create new task"

    );

    private final static Command TASK_CLEAR = new Command(
            CMD_TASK_CLEAR,
            CMD_SHORT_TASK_CLEAR,
            "Clear task list"
    );

    private final static Command TASK_LIST = new Command(
            CMD_TASK_LIST,
            CMD_SHORT_TASK_LIST,
            "Display available tasks"
    );

    private final static Command TASK_FIND_BY_ID = new Command(
            CMD_TASK_FIND_BY_ID,
            "Find task by id"
    );

    private final static Command TASK_FIND_BY_INDEX = new Command(
            CMD_TASK_FIND_BY_INDEX,
            "Find task by index"
    );

    private final static Command TASK_UPDATE_BY_ID = new Command(
            CMD_TASK_UPDATE_BY_ID,
            "Update task by id"
    );

    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            CMD_TASK_UPDATE_BY_INDEX,
            "Update task by index"
    );

    private final static Command TASK_REMOVE_BY_ID = new Command(
            CMD_TASK_REMOVE_BY_ID,
            "Remove task by id"
    );

    private final static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            CMD_TASK_CHANGE_STATUS_BY_ID,
            "Change task status by id"
    );

    private final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            CMD_TASK_CHANGE_STATUS_BY_INDEX,
            "Change task status by index"
    );

    private final static Command TASK_START_BY_ID = new Command(
            CMD_TASK_START_BY_ID,
            "Start task by id"
    );

    private final static Command TASK_START_BY_INDEX = new Command(
            CMD_TASK_START_BY_INDEX,
            "Start task by index"
    );

    private final static Command TASK_COMPLETE_BY_ID = new Command(
            CMD_TASK_COMPLETE_BY_ID,
            "Complete task by id"
    );

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(
            CMD_TASK_COMPLETE_BY_INDEX,
            "Complete task by index"
    );

    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            CMD_PROJECT_REMOVE_BY_INDEX,
            "Remove task by index"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, EXIT, INFO, ARGUMENTS, COMMANDS,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_CREATE,
            TASK_CLEAR, TASK_LIST,

            PROJECT_FIND_BY_ID, PROJECT_FIND_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_START_BY_ID,
            PROJECT_START_BY_INDEX, PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,

            TASK_FIND_BY_ID, TASK_FIND_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
