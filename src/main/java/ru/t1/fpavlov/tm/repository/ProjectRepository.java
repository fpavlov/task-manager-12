package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project) {
        this.projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        this.projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return this.projects;
    }

    @Override
    public Project findById(final String id) {
        Project itemFound = null;
        for (Project item : this.projects) {
            if (item.getId().equals(id)) {
                itemFound = item;
                break;
            }
        }
        return itemFound;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return this.projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        this.projects.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        final Project item = this.findById(id);
        if (item == null) return null;
        this.remove(item);
        return item;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project item = this.findByIndex(index);
        if (item == null) return null;
        this.remove(item);
        return item;
    }

    @Override
    public int getSize() {
        return this.projects.size();
    }

}
