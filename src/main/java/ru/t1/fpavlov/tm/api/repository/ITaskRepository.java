package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskRepository {

    Task add(Task project);

    void clear();

    List<Task> findAll();

    Task findById(final String id);

    Task findByIndex(final Integer index);

    void remove(Task task);

    Task removeById(final String id);

    Task removeByIndex(final Integer index);

    int getSize();

}
