package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.model.Command;

/*
 * Created by fpavlov on 06.10.2021.
 */
public interface ICommandService {

    Command[] getTerminalCommands();

}
