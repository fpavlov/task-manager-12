package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskService {

    Task add(Task project);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

    public void remove(final Task task);

    Task findById(final String id);

    Task findByIndex(final Integer index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    Task removeById(final String id);

    Task removeByIndex(final Integer index);

    Task changeStatusById(final String id, final Status status);

    Task changeStatusByIndex(final Integer index, final Status status);

}
